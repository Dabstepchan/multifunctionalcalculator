#ifndef THIRDDIALOG_H
#define THIRDDIALOG_H

#include <QDialog>
#include <square.h>

namespace Ui {
class ThirdDialog;
}

class ThirdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ThirdDialog(QWidget *parent = nullptr);
    ~ThirdDialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::ThirdDialog *ui;
    Square *square;

};

#endif // THIRDDIALOG_H
