QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    areasquare.cpp \
    circle.cpp \
    main.cpp \
    dialog.cpp \
    parallelogram.cpp \
    polyhedron.cpp \
    rect.cpp \
    rhombus.cpp \
    righttriangle.cpp \
    rotation.cpp \
    secdialog.cpp \
    square.cpp \
    stereometry.cpp \
    thirddialog.cpp \
    trapezoid.cpp \
    triangle.cpp

HEADERS += \
    areasquare.h \
    circle.h \
    dialog.h \
    parallelogram.h \
    polyhedron.h \
    rect.h \
    rhombus.h \
    righttriangle.h \
    rotation.h \
    secdialog.h \
    square.h \
    stereometry.h \
    thirddialog.h \
    trapezoid.h \
    triangle.h

FORMS += \
    areasquare.ui \
    circle.ui \
    dialog.ui \
    parallelogram.ui \
    polyhedron.ui \
    rect.ui \
    rhombus.ui \
    righttriangle.ui \
    rotation.ui \
    secdialog.ui \
    square.ui \
    stereometry.ui \
    thirddialog.ui \
    trapezoid.ui \
    triangle.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target
