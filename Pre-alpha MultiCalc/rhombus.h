#ifndef RHOMBUS_H
#define RHOMBUS_H

#include <QDialog>

namespace Ui {
class Rhombus;
}

class Rhombus : public QDialog
{
    Q_OBJECT

public:
    explicit Rhombus(QWidget *parent = nullptr);
    ~Rhombus();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

private:
    Ui::Rhombus *ui;
};

#endif // RHOMBUS_H
