#include "circle.h"
#include "ui_circle.h"

Circle::Circle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Circle)
{
    ui->setupUi(this);
}

Circle::~Circle()
{
    delete ui;
}

void Circle::on_pushButton_clicked()
{
    int Radius = ui->lineEdit->text().toInt();
    ui->lineEdit_2->setText(QString::number(3.14159265358*Radius*Radius));
}


void Circle::on_pushButton_2_clicked()
{
    int Diameter = ui->lineEdit_3->text().toInt();
    ui->lineEdit_4->setText(QString::number(3.14159265358*((Diameter*Diameter)/4)));
}


void Circle::on_pushButton_3_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
}

