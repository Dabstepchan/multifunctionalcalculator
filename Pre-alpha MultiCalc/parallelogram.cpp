#include "parallelogram.h"
#include "ui_parallelogram.h"

Parallelogram::Parallelogram(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Parallelogram)
{
    ui->setupUi(this);
}

Parallelogram::~Parallelogram()
{
    delete ui;
}

void Parallelogram::on_pushButton_clicked()
{
    int Height = ui->lineEdit->text().toInt();
    int Width = ui->lineEdit_2->text().toInt();
    ui->lineEdit_3->setText(QString::number(Height*Width));
}


void Parallelogram::on_pushButton_3_clicked()
{
    int SideA = ui->lineEdit_7->text().toInt();
    int SideB = ui->lineEdit_8->text().toInt();
    int Angle = ui->lineEdit_9->text().toInt();
    ui->lineEdit_10->setText(QString::number(SideA*SideB*sin(Angle*3.14159265358/180)));
}


void Parallelogram::on_pushButton_2_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_7->setText("");
    ui->lineEdit_8->setText("");
    ui->lineEdit_9->setText("");
    ui->lineEdit_10->setText("");

}

