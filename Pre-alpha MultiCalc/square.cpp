#include "square.h"
#include "ui_square.h"
#include <QMessageBox>

Square::Square(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Square)
{
    ui->setupUi(this);
}

Square::~Square()
{
    delete ui;
}

void Square::on_pushButton_clicked()
{
    close();
    areasquare = new Areasquare(this);
    areasquare->show();
}

void Square::on_pushButton_2_clicked()
{
    close();
    rect =  new Rect(this);
    rect->show();
}


void Square::on_pushButton_3_clicked()
{
    close();
    parallelogram = new Parallelogram(this);
    parallelogram->show();
}


void Square::on_pushButton_4_clicked()
{
    close();
    rhombus = new Rhombus(this);
    rhombus->show();
}


void Square::on_pushButton_5_clicked()
{
    close();
    triangle = new Triangle(this);
    triangle->show();

}


void Square::on_pushButton_6_clicked()
{
    close();
    righttriangle = new RightTriangle(this);
    righttriangle->show();
}


void Square::on_pushButton_7_clicked()
{
    close();
    trapezoid = new Trapezoid(this);
    trapezoid->show();
}


void Square::on_pushButton_8_clicked()
{
    close();
    circle = new Circle(this);
    circle->show();
}

