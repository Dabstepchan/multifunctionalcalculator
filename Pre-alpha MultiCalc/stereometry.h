#ifndef STEREOMETRY_H
#define STEREOMETRY_H

#include <QDialog>
#include <polyhedron.h>
#include <rotation.h>

namespace Ui {
class Stereometry;
}

class Stereometry : public QDialog
{
    Q_OBJECT

public:
    explicit Stereometry(QWidget *parent = nullptr);
    ~Stereometry();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Stereometry *ui;
    Polyhedron *polyhedron;
    Rotation *rotation;
};

#endif // STEREOMETRY_H
