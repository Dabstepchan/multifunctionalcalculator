#ifndef PARALLELOGRAM_H
#define PARALLELOGRAM_H

#include <QDialog>

namespace Ui {
class Parallelogram;
}

class Parallelogram : public QDialog
{
    Q_OBJECT

public:
    explicit Parallelogram(QWidget *parent = nullptr);
    ~Parallelogram();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Parallelogram *ui;
};

#endif // PARALLELOGRAM_H
