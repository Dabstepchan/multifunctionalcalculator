#ifndef POLYHEDRON_H
#define POLYHEDRON_H

#include <QDialog>
namespace Ui {
class Polyhedron;
}

class Polyhedron : public QDialog
{
    Q_OBJECT

public:
    explicit Polyhedron(QWidget *parent = nullptr);
    ~Polyhedron();

private slots:

private:
    Ui::Polyhedron *ui;
};

#endif // POLYHEDRON_H
