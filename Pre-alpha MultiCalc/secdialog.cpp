#include "secdialog.h"
#include "ui_secdialog.h"
#include <QMessageBox>

SecDialog::SecDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SecDialog)
{
    ui->setupUi(this);
}

SecDialog::~SecDialog()
{
    delete ui;
}

void SecDialog::on_pushButton_clicked()
{
    close();
    thirdDialog = new ThirdDialog(this);
    thirdDialog->show();
}



void SecDialog::on_pushButton_2_clicked()
{
    close();
    stereometry = new Stereometry(this);
    stereometry->show();
}


void SecDialog::on_pushButton_3_clicked()
{
    close();
}

