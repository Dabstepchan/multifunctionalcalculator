#include "triangle.h"
#include "ui_triangle.h"

Triangle::Triangle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Triangle)
{
    ui->setupUi(this);
}

Triangle::~Triangle()
{
    delete ui;
}

void Triangle::on_pushButton_clicked()
{
    int Side = ui->lineEdit->text().toInt();
    int Width = ui->lineEdit_2->text().toInt();
    ui->lineEdit_3->setText(QString::number(0.5*Side*Width));
}


void Triangle::on_pushButton_2_clicked()
{
    int SideA = ui->lineEdit_4->text().toInt();
    int SideB = ui->lineEdit_5->text().toInt();
    int Angle = ui->lineEdit_6->text().toInt();
    ui->lineEdit_7->setText(QString::number(0.5*SideA*SideB*sin(Angle*3.14159265358/180)));
}


void Triangle::on_pushButton_3_clicked()
{
    int SideA = ui->lineEdit_8->text().toInt();
    int SideB = ui->lineEdit_9->text().toInt();
    int SideC = ui->lineEdit_10->text().toInt();
    int Radius = ui->lineEdit_11->text().toInt();
    int HalfP = (SideA + SideB + SideC) / 2;
    ui->lineEdit_12->setText(QString::number(HalfP));
    ui->lineEdit_13->setText(QString::number(HalfP*Radius));
}


void Triangle::on_pushButton_4_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
    ui->lineEdit_6->setText("");
    ui->lineEdit_7->setText("");
    ui->lineEdit_8->setText("");
    ui->lineEdit_9->setText("");
    ui->lineEdit_10->setText("");
    ui->lineEdit_11->setText("");
    ui->lineEdit_12->setText("");
    ui->lineEdit_13->setText("");
}

