#ifndef SQUARE_H
#define SQUARE_H

#include <QDialog>
#include <areasquare.h>
#include <rect.h>
#include <parallelogram.h>
#include <rhombus.h>
#include <triangle.h>
#include <righttriangle.h>
#include <trapezoid.h>
#include <circle.h>

namespace Ui {
class Square;
}

class Square : public QDialog
{
    Q_OBJECT

public:
    explicit Square(QWidget *parent = nullptr);
    ~Square();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_6_clicked();

    void on_pushButton_7_clicked();

    void on_pushButton_8_clicked();

private:
    Ui::Square *ui;
    Areasquare *areasquare;
    Rect *rect;
    Parallelogram *parallelogram;
    Rhombus *rhombus;
    Triangle *triangle;
    RightTriangle *righttriangle;
    Trapezoid *trapezoid;
    Circle *circle;
};

#endif // SQUARE_H
