#ifndef AREASQUARE_H
#define AREASQUARE_H

#include <QDialog>

namespace Ui {
class Areasquare;
}

class Areasquare : public QDialog
{
    Q_OBJECT

public:
    explicit Areasquare(QWidget *parent = nullptr);
    ~Areasquare();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Areasquare *ui;
};

#endif // AREASQUARE_H
