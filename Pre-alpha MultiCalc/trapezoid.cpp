#include "trapezoid.h"
#include "ui_trapezoid.h"

Trapezoid::Trapezoid(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Trapezoid)
{
    ui->setupUi(this);
}

Trapezoid::~Trapezoid()
{
    delete ui;
}

void Trapezoid::on_pushButton_clicked()
{
    int BaseA = ui->lineEdit->text().toInt();
    int BaseB = ui->lineEdit_2->text().toInt();
    int Height = ui->lineEdit_3->text().toInt();
    ui->lineEdit_4->setText(QString::number(((BaseA+BaseB)/2)*Height));
}


void Trapezoid::on_pushButton_2_clicked()
{
    int D1 = ui->lineEdit_5->text().toInt();
    int D2 = ui->lineEdit_6->text().toInt();
    int Angle = ui->lineEdit_7->text().toInt();
    ui->lineEdit_8->setText(QString::number(0.5*D1*D2*sin(Angle*3.14159265358/180)));
}


void Trapezoid::on_pushButton_3_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
    ui->lineEdit_6->setText("");
    ui->lineEdit_7->setText("");
    ui->lineEdit_8->setText("");
}

