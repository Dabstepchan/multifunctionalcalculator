#include "stereometry.h"
#include "ui_stereometry.h"

Stereometry::Stereometry(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Stereometry)
{
    ui->setupUi(this);
}

Stereometry::~Stereometry()
{
    delete ui;
}

void Stereometry::on_pushButton_clicked()
{
    close();
    polyhedron = new Polyhedron(this);
    polyhedron->show();
}


void Stereometry::on_pushButton_2_clicked()
{
    close();
    rotation = new Rotation(this);
    rotation->show();
}


