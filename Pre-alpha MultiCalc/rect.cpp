#include "rect.h"
#include "ui_rect.h"

Rect::Rect(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Rect)
{
    ui->setupUi(this);
}

Rect::~Rect()
{
    delete ui;
}

void Rect::on_pushButton_clicked()
{
    int Height = ui->lineEdit->text().toInt();
    int Width = ui->lineEdit_2->text().toInt();
    ui->lineEdit_3->setText(QString::number(Height*Width));
}


void Rect::on_pushButton_2_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
}

