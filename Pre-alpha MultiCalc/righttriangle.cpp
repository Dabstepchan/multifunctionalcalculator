#include "righttriangle.h"
#include "ui_righttriangle.h"

RightTriangle::RightTriangle(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::RightTriangle)
{
    ui->setupUi(this);
}

RightTriangle::~RightTriangle()
{
    delete ui;
}

void RightTriangle::on_pushButton_clicked()
{
    int SideA = ui->lineEdit->text().toInt();
    int SideB = ui->lineEdit_2->text().toInt();
    ui->lineEdit_3->setText(QString::number(0.5*SideA*SideB));
}


void RightTriangle::on_pushButton_2_clicked()
{
    int Hypotenuse = ui->lineEdit_4->text().toInt();
    int Height = ui->lineEdit_5->text().toInt();
    ui->lineEdit_6->setText(QString::number(0.5*Hypotenuse*Height));
}


void RightTriangle::on_pushButton_3_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
    ui->lineEdit_6->setText("");
}

