#ifndef RIGHTTRIANGLE_H
#define RIGHTTRIANGLE_H

#include <QDialog>

namespace Ui {
class RightTriangle;
}

class RightTriangle : public QDialog
{
    Q_OBJECT

public:
    explicit RightTriangle(QWidget *parent = nullptr);
    ~RightTriangle();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::RightTriangle *ui;
};

#endif // RIGHTTRIANGLE_H
