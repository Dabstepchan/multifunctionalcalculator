#ifndef CIRCLE_H
#define CIRCLE_H

#include <QDialog>

namespace Ui {
class Circle;
}

class Circle : public QDialog
{
    Q_OBJECT

public:
    explicit Circle(QWidget *parent = nullptr);
    ~Circle();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Circle *ui;
};

#endif // CIRCLE_H
