#include "rhombus.h"
#include "ui_rhombus.h"

Rhombus::Rhombus(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Rhombus)
{
    ui->setupUi(this);
}

Rhombus::~Rhombus()
{
    delete ui;
}

void Rhombus::on_pushButton_clicked()
{
    int SideA = ui->lineEdit->text().toInt();
    int Height = ui->lineEdit_2->text().toInt();
    ui->lineEdit_3->setText(QString::number(Height*SideA));
}


void Rhombus::on_pushButton_2_clicked()
{
    int SideA = ui->lineEdit_4->text().toInt();
    int Angle = ui->lineEdit_5->text().toInt();
    ui->lineEdit_6->setText(QString::number(SideA*SideA*sin(Angle*3.14159265358/180)));
}


void Rhombus::on_pushButton_3_clicked()
{
    int D1 = ui->lineEdit_7->text().toInt();
    int D2 = ui->lineEdit_8->text().toInt();
    ui->lineEdit_9->setText(QString::number(0.5*D1*D2));
}


void Rhombus::on_pushButton_4_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
    ui->lineEdit_3->setText("");
    ui->lineEdit_4->setText("");
    ui->lineEdit_5->setText("");
    ui->lineEdit_6->setText("");
    ui->lineEdit_7->setText("");
    ui->lineEdit_8->setText("");
    ui->lineEdit_9->setText("");
}

