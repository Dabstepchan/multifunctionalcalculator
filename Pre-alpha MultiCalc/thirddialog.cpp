#include "thirddialog.h"
#include "ui_thirddialog.h"
#include <QMessageBox>

ThirdDialog::ThirdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ThirdDialog)
{
    ui->setupUi(this);
}

ThirdDialog::~ThirdDialog()
{
    delete ui;
}

void ThirdDialog::on_pushButton_clicked()
{
    close();
    square = new Square(this);
    square->show();
}

