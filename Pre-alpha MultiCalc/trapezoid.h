#ifndef TRAPEZOID_H
#define TRAPEZOID_H

#include <QDialog>

namespace Ui {
class Trapezoid;
}

class Trapezoid : public QDialog
{
    Q_OBJECT

public:
    explicit Trapezoid(QWidget *parent = nullptr);
    ~Trapezoid();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

private:
    Ui::Trapezoid *ui;
};

#endif // TRAPEZOID_H
