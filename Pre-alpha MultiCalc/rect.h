#ifndef RECT_H
#define RECT_H

#include <QDialog>

namespace Ui {
class Rect;
}

class Rect : public QDialog
{
    Q_OBJECT

public:
    explicit Rect(QWidget *parent = nullptr);
    ~Rect();

private slots:
    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Rect *ui;
};

#endif // RECT_H
