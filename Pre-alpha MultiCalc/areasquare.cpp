#include "areasquare.h"
#include "ui_areasquare.h"

Areasquare::Areasquare(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Areasquare)
{
    ui->setupUi(this);
}

Areasquare::~Areasquare()
{
    delete ui;
}

void Areasquare::on_pushButton_clicked()
{
    QString input = ui->lineEdit->text();
    int final = input.toInt()*input.toInt();
    ui->lineEdit_2->setText(QString::number(final));
}


void Areasquare::on_pushButton_2_clicked()
{
    ui->lineEdit->setText("");
    ui->lineEdit_2->setText("");
}

