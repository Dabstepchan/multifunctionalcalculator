#ifndef ROTATION_H
#define ROTATION_H

#include <QDialog>

namespace Ui {
class Rotation;
}

class Rotation : public QDialog
{
    Q_OBJECT

public:
    explicit Rotation(QWidget *parent = nullptr);
    ~Rotation();

private:
    Ui::Rotation *ui;
};

#endif // ROTATION_H
