#ifndef SQUARE_H
#define SQUARE_H

#include <QDialog>

namespace Ui {
class Square;
}

class Square : public QDialog
{
    Q_OBJECT

public:
    explicit Square(QWidget *parent = nullptr);
    ~Square();

private:
    Ui::Square *ui;
};

#endif // SQUARE_H
