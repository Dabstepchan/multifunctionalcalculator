#include "polyhedron.h"
#include "ui_polyhedron.h"

Polyhedron::Polyhedron(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Polyhedron)
{
    ui->setupUi(this);
}

Polyhedron::~Polyhedron()
{
    delete ui;
}
