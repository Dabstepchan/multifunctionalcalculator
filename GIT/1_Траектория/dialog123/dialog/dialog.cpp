#include "dialog.h"
#include "ui_dialog.h"
#include <QMessageBox>

dialog::dialog(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::dialog)
{
    ui->setupUi(this);
}

dialog::~dialog()
{
    delete ui;
}


void dialog::on_pushButton_clicked()
{
    hide();
    secDialog = new SecDialog(this);
    secDialog->show();
}

