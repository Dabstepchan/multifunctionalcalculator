#ifndef DIALOG_H
#define DIALOG_H

#include <QMainWindow>
#include "secdialog.h"

QT_BEGIN_NAMESPACE
namespace Ui { class dialog; }
QT_END_NAMESPACE

class dialog : public QMainWindow
{
    Q_OBJECT

public:
    dialog(QWidget *parent = nullptr);
    ~dialog();

private slots:
    void on_pushButton_clicked();

private:
    Ui::dialog *ui;
    SecDialog *secDialog;
};
#endif // DIALOG_H
