#ifndef THIRDDIALOG_H
#define THIRDDIALOG_H

#include <QDialog>
#include <polyhedron.h>
#include <rotation.h>
#include <square.h>

namespace Ui {
class ThirdDialog;
}

class ThirdDialog : public QDialog
{
    Q_OBJECT

public:
    explicit ThirdDialog(QWidget *parent = nullptr);
    ~ThirdDialog();

private slots:
    void on_pushButton_clicked();
    void on_pushButton_2_clicked();
    void on_pushButton_3_clicked();

private:
    Ui::ThirdDialog *ui;
    Polyhedron *polyhedron;
    Rotation *rotation;
    Square *square;

};

#endif // THIRDDIALOG_H
