#include "thirddialog.h"
#include "ui_thirddialog.h"
#include <QMessageBox>

ThirdDialog::ThirdDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::ThirdDialog)
{
    ui->setupUi(this);
}

ThirdDialog::~ThirdDialog()
{
    delete ui;
}

void ThirdDialog::on_pushButton_clicked()
{
    hide();
    square = new Square(this);
    square->show();
}

void ThirdDialog::on_pushButton_2_clicked()
{
    hide();
    polyhedron = new Polyhedron(this);
    polyhedron->show();
}

void ThirdDialog::on_pushButton_3_clicked()
{
    hide();
    rotation = new Rotation(this);
    rotation->show();
}
