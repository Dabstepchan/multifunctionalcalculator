#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->radioButton_2->setChecked(true);
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::UpdateFruits()
{
    if(ui->radioButton_2->isChecked()){
        ui->comboBox->model()->sort(0,Qt::AscendingOrder);
    }
    else
    {
        ui->comboBox->model()->sort(0,Qt::DescendingOrder);
    }
    int index = ui ->comboBox->currentIndex();

    ui->label->setText(QString::number(index)+' ' + ui->comboBox->currentText());
}


void MainWindow::on_radioButton_2_toggled(bool checked)
{
    UpdateFruits();
}


void MainWindow::on_radioButton_toggled(bool checked)
{
    UpdateFruits();
}


void MainWindow::on_comboBox_currentIndexChanged(int index)
{
    ui->label->setText(QString::number(index)+' ' + ui->comboBox->currentText());
}

