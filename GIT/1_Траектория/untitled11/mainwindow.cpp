#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "secondwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    connect(ui->pushButton,SIGNAL(clicked(bool)),this,SLOT(onButtonClicked()));
    connect(ui->pushButton_2,SIGNAL(clicked(bool)),this,SLOT(onButtonClicked_2()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::onButtonClicked()
{
    ui->label->setText(tr("123"));
}

void MainWindow::onButtonClicked_2()
{
     ui->label->setText(tr("456"));
}


void MainWindow::on_pushButton_3_clicked()
{
    hide();
    SecondWindow window;
    window.setModal(true);
    window.exec();
}

